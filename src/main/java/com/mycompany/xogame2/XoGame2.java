/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xogame2;
import java.util.Scanner;
/**
 *
 * @author MisterNrazZ
 */
public class XoGame2 {

    static char[][] table = {{'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}};
    //static int[] winLine = {'123','456','789','147','258','369','159','357'};
    static char player = 'O';
    static int num;
    static char numString;

    public static void main(String[] args) {

        printWelcome();
        int i = 0 ;
        while (i < 9) {
            printTable();
            printTurn();
            inputNum();
            if (isWin()) {
                printTable();
                printWin();
                break;
            }
            
            
            switchPlayer();
            i++ ;
        }
        printDawn();  

    }

    private static void printWelcome() {
        System.out.println("Welcome OX");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void printTurn() {
        System.out.println(player + " Trun");
    }

    private static void inputNum() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input number:");
        num = kb.nextInt();
        numString = (char) num;
        if (num <= 9) {
            if (num == 1) {
                table[0][0] = player;
            } else if (num == 2) {
                table[0][1] = player;
            } else if (num == 3) {
                table[0][2] = player;
            } else if (num == 4) {
                table[1][0] = player;
            } else if (num == 5) {
                table[1][1] = player;
            } else if (num == 6) {
                table[1][2] = player;
            } else if (num == 7) {
                table[2][0] = player;
            } else if (num == 8) {
                table[2][1] = player;
            } else if (num == 9) {
                table[2][2] = player;
            }

        }

    }

    private static void switchPlayer() {
        if (player == 'O') {
            player = 'X';
        } else {
            player = 'O';
        }

    }

    private static boolean isWin() {
        if (check()) {
            return true;
        }
        return false;
    }

    private static void printWin() {
        System.out.println(player + " Win");
    }
    

    private static boolean check() {
        if (table[0][0] == table[1][1] && table[0][0] == table[2][2]) {
            return true;
        } else if (table[0][2] == table[1][1] && table[0][2] == table[2][0]) {
            return true;

        } else if (table[0][0] == table[0][1] && table[0][0] == table[0][2]) {
            return true;
        } else if (table[1][0] == table[1][1] && table[1][0] == table[1][2]) {
            return true;
        } else if (table[2][0] == table[2][1] && table[2][0] == table[2][2]) {
            return true;

        } else if (table[0][0] == table[1][0] && table[0][0] == table[2][0]) {
            return true;
        } else if (table[0][1] == table[1][1] && table[0][1] == table[2][1]) {
            return true;
        } else if (table[0][2] == table[1][2] && table[0][2] == table[2][2]) {
            return true;
        }
        return false;

    }

    private static void printDawn() {
        System.out.print("Dawn");
    }

    
    

}
